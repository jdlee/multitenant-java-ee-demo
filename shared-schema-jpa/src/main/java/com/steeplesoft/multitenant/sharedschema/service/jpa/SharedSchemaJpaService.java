/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.sharedschema.service.jpa;

import com.steeplesoft.multitenant.model.CustomerOrder;
import com.steeplesoft.multitenant.service.MultitenantService;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 *
 * @author jdlee
 */
@Default
//@Alternative
@RequestScoped
public class SharedSchemaJpaService implements MultitenantService {
    @Inject
    private EntityManager em;

    @Override
    public List<CustomerOrder> getOrders() {
        return em.createNamedQuery("getAllOrders", CustomerOrder.class).getResultList();
    }

    @Override
    @Transactional
    public CustomerOrder createOrder(Long custId) {
        CustomerOrder co = new CustomerOrder();
        try {
            co.setCustomer(custId);
            co.setOrderDate(new Date());
            em.persist(co);
            em.flush();

            // Force reload of the entity to get the company ID set by the trigger
            TypedQuery<CustomerOrder> query = 
                    em.createNamedQuery("loadOrderById", CustomerOrder.class)
                            .setHint("javax.persistence.cache.storeMode", "REFRESH")
                            .setParameter(1, co.getId());
            co = query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return co;
    }
}
