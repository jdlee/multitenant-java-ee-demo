package com.steeplesoft.multitenant.sharedschema.service.jpa;

import java.security.Principal;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author jdlee
 */
@RequestScoped
public class EntityManagerProvider {
    @PersistenceContext(unitName = "em")
    EntityManager em;

    @Inject
    private Principal principal;

//    @Context
//    SecurityContext securityContext;

    @Produces
    @PersistenceContext
    public EntityManager getEntityManager() {
        Query cIdQuery = em.createNativeQuery("select comp_id from allusers where email = ?");
        cIdQuery.setParameter(1, principal.getName());
        try {
            final Object singleResult = cIdQuery.getSingleResult();
            String compId = singleResult.toString();

            if (compId != null) {
                Query query = em.createNativeQuery("SELECT set_var('comp_id', ?1)");
                query.setParameter(1, compId);
                query.getSingleResult();
            }
        } catch (javax.persistence.NoResultException nre) {
            nre.printStackTrace();
        }
        return em;
    }
}
