/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.sharedschema.jpa;

import com.steeplesoft.multitenant.MultitenantTest;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.WebArchive;

/**
 *
 * @author jdlee
 */
public class MultitenantSharedSchemaJpaTest extends MultitenantTest {
    @Deployment
    public static WebArchive createDeployment() {
        return MultitenantTest.createBaseDeployment()
                .addAsResource("META-INF/persistence.xml");
    }
    
}
