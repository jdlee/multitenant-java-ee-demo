DROP TABLE IF EXISTS alluser_groups;
DROP TABLE IF EXISTS allgroups;
DROP TABLE IF EXISTS allusers;

CREATE TABLE allcompanies
(
    id INT PRIMARY KEY,
    name VARCHAR(100),
    dbschema VARCHAR(100),
    dbname VARCHAR(100)
);

CREATE TABLE allusers
(
    id SERIAL PRIMARY KEY,
    comp_id INT NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(45) NOT NULL UNIQUE,
    password VARCHAR(100),
    FOREIGN KEY (comp_id) REFERENCES allcompanies(id)
);

CREATE TABLE allgroups
(
    id SERIAL PRIMARY KEY,
    comp_id INT NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(300),
    UNIQUE (comp_id, name),
    FOREIGN KEY (comp_id) REFERENCES allcompanies(id)
);

CREATE TABLE alluser_groups
(
    group_id int NOT NULL,
    email VARCHAR(255) NOT NULL,
    UNIQUE(group_id, email),
    FOREIGN KEY (group_id) REFERENCES allgroups(id),
    FOREIGN KEY (email) REFERENCES allusers(email)
);

CREATE INDEX SQL_USERGROUPS_EMAIL_INDEX ON alluser_groups(email);
CREATE INDEX SQL_USERgroup_id_INDEX ON alluser_groups(group_id);

INSERT INTO allcompanies (id, name, dbschema, dbname) VALUES (1, 'The Hardy Boys', 'comp1', 'mtcomp1');
INSERT INTO allcompanies (id, name, dbschema, dbname) VALUES (2, 'Baker Street PI', 'comp2', 'mtcomp2');

INSERT INTO allusers (id, comp_id, first_name, last_name, email, password) 
    VALUES (1000, 1, 'Frank', 'Hardy', 'frank@thehardies.com', '5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8');
INSERT INTO allusers (id, comp_id, first_name, last_name, email, password) 
    VALUES (1001, 1, 'Joe', 'Hardy', 'joe@thehardies.com', '5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8');
INSERT INTO allusers (id, comp_id, first_name, last_name, email, password) 
    VALUES (1002, 2, 'Sherlock', 'Holmes', 'im@genius.com', '5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8');
INSERT INTO allusers (id, comp_id, first_name, last_name, email, password) 
    VALUES (1003, 2, 'Jason', 'Lee', 'imnot@genius.com', '5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8');

INSERT INTO allgroups (id, comp_id, name) VALUES (1001, 1, 'Admin');
INSERT INTO allgroups (id, comp_id, name) VALUES (1002, 1, 'Users');
INSERT INTO allgroups (id, comp_id, name) VALUES (1003, 2, 'Admin');
INSERT INTO allgroups (id, comp_id, name) VALUES (1004, 2, 'Users');

INSERT INTO alluser_groups VALUES (1001, 'frank@thehardies.com');
INSERT INTO alluser_groups VALUES (1001, 'joe@thehardies.com');
INSERT INTO alluser_groups VALUES (1003, 'im@genius.com');
INSERT INTO alluser_groups VALUES (1004, 'imnot@genius.com');

