<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>Cloudy with a Chance of Java: Building Multitenant Java EE Cloud Applications</title>

        <meta name="description" content="A discussion/demo of techniques for multitenancy at the database level in a Java EE application.">
        <meta name="author" content="Jason Lee">

        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

        <link rel="stylesheet" href="css/reveal.css">
        <link rel="stylesheet" href="css/theme/black.css" id="theme">

        <!-- Code syntax highlighting -->
        <link rel="stylesheet" href="lib/css/zenburn.css">

        <!-- Printing and PDF exports -->
        <script>
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = window.location.search.match(/print-pdf/gi) ? 'css/print/pdf.css' : 'css/print/paper.css';
            document.getElementsByTagName('head')[0].appendChild(link);
        </script>

        <!--[if lt IE 9]>
        <script src="lib/js/html5shiv.js"></script>
        <![endif]-->
        <style type="text/css">
            .reveal h1, .reveal h2, .reveal h3, .reveal h4, .reveal h5, .reveal h6 {
                text-transform: capitalize
            }
            
            .reveal .slide-background {
                background-image: url("img/ns-logo.png");
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: bottom left;              
                background-size: 5%;
            }
        </style>
    </head>

    <body>
        <div class="reveal">
            <div class="slides" style="width: 1500px">
                <section>
                    <div style="text-align: left;">
                        <h1>Cloudy with a Chance of Java</h1>
                        <h2>Building Multitenant Java EE Cloud Applications</h2>
                        <br>
                        <h3>Jason Lee</h3>
                        <h4>Principal Software Engineer, NetSuite, Inc.</h4>
                    </div>
                </section>
                <section>
                    <h1>Agenda</h1>
                    <ol>
                        <li>Introduction</li>
                        <li>Overview of Strategies</li>
                        <li>Implementations</li>
                        <li>Q&A</li>
                    </ol>
                </section>
                <section>
                    <h1>Overview of Strategies</h1>
                    <ul>
                        <li>Separate Databases</li>
                        <li>Shared Database, Separate Schemas</li>
                        <li>Shared Database, Shared Schema</li>
                        <li>Several others possible, not worth mentioning
                            <ul>
                                <li>Shared Database, shared schema, separate tables</li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <section>
                    <h1>Separate Databases</h1>
                    <section>
                        <br>
                        Each tenant has its own copy of the entire database
                        <img style="float: right" src="img/sepdb.png" height="450" alt="One server, multiple instances">
                    </section>
                    <section>
                        <br>
                        Each database can be in its own instance, or the databases can share an instance
                        <img style="float: right" src="img/multserv.png" height="450" alt="Multiple servers">
                    </section>
                    <section data-style="top: 0px">
                        <div style="text-align: left; left: 25%">
                            <h2>Advantages</h2>
                            <ul>
                                <li class="fragment">Customer data is 100% isolated</li>
                                <li class="fragment">Backup/Recovery is simplified</li>
                            </ul>
                            <h2 class="fragment">Disadvantages</h2>
                            <ul>
                                <li class="fragment">Licensing costs may increase</li>
                                <li class="fragment">Infrastructure costs will almost certainly increase</li>
                                <li class="fragment">Application updates are more difficult</li>
                            </ul>
                        </div>
                    </section>
                </section>
                <section>
                    <h1>Shared Database, Separate Schema</h1>
                    <section>
                        <br>
                        Each tenant is stored in the same database, but each gets its own schema
                        <img style="float: right" src="img/sepschema.png" height="450" alt="Separate Schema">
                    </section>
                    <section>
                        <div style="text-align: left; left: 25%">
                            <h2>Advantages</h2>
                            <ul>
                                <li class="fragment">Customer data is mostly isolated</li>
                                <li class="fragment">Backup/Recovery is simplified</li>
                                <li class="fragment">Licensing costs likely unaffected</li>
                            </ul>
                            <h2 class="fragment">Disadvantages</h2>
                            <ul>
                                <li class="fragment">Database and application configuration is more complex</li>
                                <li class="fragment">Application updates are more difficult</li>
                                <li class="fragment">Storage space grows more quickly</li>
                            </ul>
                        </div>
                    </section>
                </section>
                <section>
                    <h1>Shared Database, Shared Schema</h1>
                    <section>
                        <br>
                        <img style="float: right" src="img/sharedschema.png" height="450" alt="Shared Schema">
                        <div style="float: left; width: 65%">
                            <ul>
                                <li class="fragment">Every tenant is in the same database and in the same schema.</li>
                                <li class="fragment">A discriminator field (e.g., tenant_id, comp_id) is used to logically separate customer data.</li>
                                <li class="fragment">All customer data is in the same set of tables, so care must be taken not to expose the wrong data to a customer.</li>
                            </ul>
                        </div>
                    </section>
                    <section>
                        <br>
                        <div style="text-align: left; left: 25%">
                            <h2 class="fragment">Advantages</h2>
                            <ul>
                                <li class="fragment">Database configuration is very simple</li>
                                <li class="fragment">Licensing costs likely unaffected</li>
                                <li class="fragment">Application updates are simplified</li>
                                <li class="fragment">Infrastructure costs increase more slowly</li>
                            </ul>
                            <h2 class="fragment">Disadvantages</h2>
                            <ul>
                                <li class="fragment">Customer data is not physically isolated</li>
                                <li class="fragment">Backup/restore is much more difficult</li>
                                <li class="fragment">Database and application configuration can be more complex</li>
                            </ul>
                        </div>
                    </section>
                    <section>
                        <div style="text-align: left; left: 25%;">
                            <h2>Application approaches</h2>
                            <ul>
                                <li class="fragment">Every time the database is queried, the application must specify the discriminator value</li>
                                <li class="fragment">The application works with specially-constructed views that return data for only the current tenant.</li>
                            </ul>
                        </div>
                    </section>
                </section>
                <section>
                    <h1>Demo</h1>
                    <table>
                        <tr>
                            <td><img src="img/payara-logo.png" alt="Payara Server Logo" style="border: 0px;" height="125"></td>
                            <td><img src="img/netbeans-logo.png" alt="NetBeans Logo" style="border: 0px;" height="125"></td>
                            <td><img src="img/postgresql-logo.png" alt="PostgreSQL Logo" style="border: 0px;" height="125"></td>
                        </tr>
                    </table>
                </section>
                <section>
                    <h1>Security Tables</h1>
                    Basic, but useable example
                    <img style="float: right" src="img/secerd.png" height="450" alt="Security ERD">
                </section>
                <section>
                    <h1>Q&A</h1>
                </section>
                <section>
                    <h1>Resources</h1>
                    <ul>
                        <li>Source Code: 
                            <ul>
                                <li>https://bitbucket.org/jdlee/multitenant-java-ee-demo</li>
                            </ul>
                        </li>
                        <li>Recommended Reading
                            <ul>
                                <li>https://msdn.microsoft.com/en-us/library/aa479086.aspx</li>
                                <li>http://www.ibm.com/developerworks/data/library/techarticle/dm-1201dbdesigncloud/</li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <section>
                    <h1>Thank You</h1>
                </section>
            </div>
        </div>

        <script src="lib/js/head.min.js"></script>
        <script src="js/reveal.js"></script>
        <script>
            Reveal.initialize({
                width: "100%",
                controls: true,
                progress: true,
                history: true,
                center: true,
                transition: 'slide', // none/fade/slide/convex/concave/zoom

                // Optional reveal.js plugins
                dependencies: [
                    {src: 'lib/js/classList.js', condition: function () {
                            return !document.body.classList;
                        }},
                    {src: 'plugin/markdown/marked.js', condition: function () {
                            return !!document.querySelector('[data-markdown]');
                        }},
                    {src: 'plugin/markdown/markdown.js', condition: function () {
                            return !!document.querySelector('[data-markdown]');
                        }},
                    {src: 'plugin/highlight/highlight.js', async: true, condition: function () {
                            return !!document.querySelector('pre code');
                        }, callback: function () {
                            hljs.initHighlightingOnLoad();
                        }},
                    {src: 'plugin/zoom-js/zoom.js', async: true},
                    {src: 'plugin/notes/notes.js', async: true}
                ]
            });

        </script>
    </body>
</html>