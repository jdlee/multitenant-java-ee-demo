/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant;

import com.steeplesoft.multitenant.model.CustomerOrder;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author jdlee
 */
@RunWith(Arquillian.class)
public class MultitenantTest {

    @ArquillianResource
    private URL deploymentURL;

    public static WebArchive createBaseDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, "com.steeplesoft.multitenant")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/web.xml"))
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/glassfish-web.xml"))
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    @RunAsClient
    public void shouldNotSeeOtherCompanysData() throws URISyntaxException, JSONException {
        checkUserData("frank@thehardies.com", "password", 1);
        checkUserData("joe@thehardies.com", "password", 1);
        checkUserData("im@genius.com", "password", 2);
    }
    
    @Test
    @RunAsClient
    public void newOrdersShouldHaveCompanySet() throws URISyntaxException {
        Client client = getClient("frank@thehardies.com", "password");
        Form form = new Form();
        form.param("custId", "1010");

        WebTarget target = client.target(deploymentURL.toURI());
        Response r = target.path("api/orders")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        CustomerOrder order = r.readEntity(CustomerOrder.class);
        Assert.assertEquals(new Long(1), order.getCompanyId());
    }
    
    private void checkUserData(String userName, String password, int companyId) throws JSONException, URISyntaxException {
        Client client = getClient(userName, password);
        
        WebTarget target = client.target(deploymentURL.toURI());
        Response r = target.path("api/orders").request().get();
        Assert.assertEquals(200, r.getStatus());
        
        String text = r.readEntity(String.class);
        Assert.assertNotNull(text);
        Assert.assertTrue(text.startsWith("["));
        
        JSONArray list = new JSONArray(text);
        Assert.assertTrue(list.length() > 0);
        
        for (int i = 0; i < list.length(); i++) {
            Assert.assertEquals(companyId, list.getJSONObject(i).getInt("companyId"));
        }
    }

    private Client getClient(String userName, String password) {
        Client client = ClientBuilder.newClient()
                .register(MoxyJsonFeature.class)
                .register(HttpAuthenticationFeature.basic(userName, password));
        return client;
    }
}
