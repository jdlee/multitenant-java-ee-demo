/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.separateschema.service;

import com.steeplesoft.multitenant.service.jdbc.JdbcConnection;
import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 *
 * @author jdlee
 */
@Alternative
@RequestScoped
public class JdbcConnectionProducer {

    @Resource(mappedName = "jdbc/mtms")
    private javax.sql.DataSource dataSource;

    @Inject
    private Principal principal;

    @Produces
    @JdbcConnection
    public Connection getConnection() throws SQLException {
        Connection c = dataSource.getConnection();
        PreparedStatement stmt = 
                c.prepareStatement("select dbschema from security.allusers u, security.allcompanies c where email = ? and comp_id = c.id");
        stmt.setString(1, principal.getName());
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            final String schema = rs.getString(1);
            // Production systems would manage and cache connections more intelligently
            c.setSchema(schema);

            close(rs);
            close(stmt);
//            close(c);

            return c;
        } else {
            close(rs);
            close(stmt);
//            close(c);

            throw new RuntimeException("Unable to determine company schema.");
        }
    }

    public void disposeConnection(@Disposes @JdbcConnection Connection conn) throws SQLException {
        conn.close();
    }

    private void close(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception ex) {
                Logger.getLogger(JdbcConnectionProducer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
