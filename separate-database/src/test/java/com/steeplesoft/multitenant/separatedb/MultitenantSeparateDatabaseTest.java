/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.separatedb;

import com.steeplesoft.multitenant.MultitenantTest;
import java.io.File;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.WebArchive;

/**
 *
 * @author jdlee
 */
public class MultitenantSeparateDatabaseTest extends MultitenantTest {
    @Deployment
    public static WebArchive createDeployment() {
        return MultitenantTest.createBaseDeployment()
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/beans.xml"));
    }
    
}
