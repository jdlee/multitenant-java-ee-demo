package com.steeplesoft.multitenant.separatedb.service;

import com.steeplesoft.multitenant.model.CustomerOrder;
import com.steeplesoft.multitenant.service.MultitenantService;
import com.steeplesoft.multitenant.service.jdbc.JdbcConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

/**
 *
 * @author jdlee
 */
@Alternative
@RequestScoped
public class SeparateDatabaseService implements MultitenantService {

    @Inject
    @JdbcConnection
    private Connection conn;

    @Override
    public List<CustomerOrder> getOrders() {
        List<CustomerOrder> orders = new ArrayList<>();
        try (Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT id, comp_id, customer, orderdate FROM orders ORDER BY id")) {
            while (rs.next()) {
                CustomerOrder co = new CustomerOrder();
                co.setId(rs.getLong(1));
                co.setCompanyId(rs.getLong(2));
                co.setCustomer(rs.getLong(3));
                co.setOrderDate(rs.getDate(4));
                orders.add(co);
            }
        } catch (Exception ex) {
            Logger.getLogger(SeparateDatabaseService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return orders;
    }

    @Override
    public CustomerOrder createOrder(Long custId) {
        CustomerOrder co = new CustomerOrder();
        try (PreparedStatement ps = 
                conn.prepareStatement("INSERT INTO orders (customer, orderdate) VALUES (?, ?) RETURNING id;")) {
            co.setCustomer(custId);
            co.setOrderDate(new Date());

            ps.setLong(1, custId);
            ps.setDate(2, new java.sql.Date(co.getOrderDate().getTime()));
            try (ResultSet rs = ps.executeQuery()) {
                rs.next();
                final long id = rs.getLong(1);
                
                try (PreparedStatement ps2 = 
                        conn.prepareStatement("SELECT id, comp_id, customer, orderdate FROM orders WHERE id = ?")) {
                    ps2.setLong(1, id);
                    try (ResultSet rs2 = ps2.executeQuery()) {
                        if (rs2.next()) {
                            co.setId(rs2.getLong(1));
                            co.setCompanyId(rs2.getLong(2));
                            co.setCustomer(rs2.getLong(3));
                            co.setOrderDate(rs2.getDate(4));
                        }
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(SeparateDatabaseService.class.getName()).log(Level.SEVERE, null, ex);
            throw new WebApplicationException("Unable to save new order. See log for details.");
        }

        return co;
    }

}
