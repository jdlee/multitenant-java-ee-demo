DROP VIEW IF EXISTS orders;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS allorders;
CREATE EXTENSION IF NOT EXISTS plperl;

CREATE OR REPLACE FUNCTION set_var(name text, val text) RETURNS text AS  $$
    if ($_SHARED{$_[0]} = $_[1]) {
        return 'ok';
    } else {
        return "cannot set shared variable $_[0] to $_[1]";
    }
$$ LANGUAGE plperl;

CREATE OR REPLACE FUNCTION get_var(name text) RETURNS text AS $$
    return $_SHARED{$_[0]};
$$ LANGUAGE plperl;

CREATE OR REPLACE FUNCTION set_company() RETURNS trigger AS $$
    BEGIN
        IF NEW.comp_id IS NULL THEN
            NEW.comp_id := cast (get_var('comp_id') AS NUMERIC);
        END IF;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

