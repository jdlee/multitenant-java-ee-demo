/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.service;

import com.steeplesoft.multitenant.model.CustomerOrder;
import java.util.List;

/**
 *
 * @author jdlee
 */
public interface MultitenantService {
    List<CustomerOrder> getOrders();
    CustomerOrder createOrder(Long custId);
}
