/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.service.jdbc;

import java.security.Principal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 *
 * @author jdlee
 */
@RequestScoped
@Default
public class JdbcProducer {

    @Resource(mappedName = "jdbc/mt")
    private javax.sql.DataSource dataSource;

    @Inject
    private Principal principal;

    @Produces
    @JdbcConnection
    public Connection getConnection() throws SQLException {
        Connection c = dataSource.getConnection();
        try (PreparedStatement stmt = c.prepareStatement("select comp_id from allusers where email = ?")) {
            stmt.setString(1, principal.getName());
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    String companyId = rs.getString(1);
                    CallableStatement cs = c.prepareCall("SELECT set_var('comp_id', ?)");
                    cs.closeOnCompletion();
                    cs.setString(1, companyId);
                    cs.execute();
                } else {
                    throw new RuntimeException("Unable to determine company ID");
                }
            }
        }

        return c;
    }

    public void close(@Disposes @JdbcConnection Connection conn) throws SQLException {
        conn.close();
    }
}
