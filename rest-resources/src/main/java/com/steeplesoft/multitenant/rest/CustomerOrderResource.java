/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.multitenant.rest;

import com.steeplesoft.multitenant.model.CustomerOrder;
import com.steeplesoft.multitenant.service.MultitenantService;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 *
 * @author jdlee
 */
@Path("/orders")
@RequestScoped
public class CustomerOrderResource {
    @Inject
    MultitenantService service;

    @GET
    public List<CustomerOrder> getOrders() {
        return service.getOrders();
    }

    @POST
    public CustomerOrder createOrder(@FormParam("custId") final Long custId) {
        return service.createOrder(custId);
    }
}
