#!/bin/bash

PAYARA_URL=http://bit.ly/1IaXo67 #http://bit.ly/1I4tz9r
PAYARA_PATH=payara41/bin

if [ ! -e payara.zip ] ; then
    echo Downloading Payara
    wget -q http://bit.ly/1I4tz9r -O payara.zip
fi
if [ ! -e postgresql-jdbc.jar ] ; then
    echo Downloading the PostgreSQL JDBC driver
    wget -q http://search.maven.org/remotecontent?filepath=org/postgresql/postgresql/9.4-1201-jdbc41/postgresql-9.4-1201-jdbc41.jar -O postgresql-jdbc.jar
fi

echo Extracting Payara
if [ -e payara41/bin/asadmin ] ; then
    payara41/bin/asadmin stop-domain &>/dev/null
fi

rm -rf payara41/
unzip -q payara.zip
echo "CustomJdbcUserRealm { nl.mb.glassfish.realm.CustomJdbcLoginModule required; };" >> payara41/glassfish/domains/domain1/config/login.conf
cp postgresql-jdbc.jar payara41/glassfish/domains/domain1/lib/postgresql-jdbc.jar

echo Building the JDBC Realm
git clone https://github.com/martijnblankestijn/glassfish-jdbc-realm &> /dev/null
cd glassfish-jdbc-realm
git pull &> /dev/null
mvn package &> /dev/null
cd ..

echo Starting the server
$PAYARA_PATH/asadmin start-domain --debug domain1 &> server.log 

sleep 3

echo Configuring the server
$PAYARA_PATH/asadmin delete-jdbc-resource jdbc/mt &>/dev/null
$PAYARA_PATH/asadmin delete-jdbc-resource jdbc/mtms &>/dev/null
$PAYARA_PATH/asadmin delete-jdbc-connection-pool MtPool &>/dev/null
$PAYARA_PATH/asadmin delete-jdbc-connection-pool MtmsPool &>/dev/null
$PAYARA_PATH/asadmin delete-auth-realm MultitenantRealm &>/dev/null
$PAYARA_PATH/asadmin delete-auth-realm MultitenantMultiSchemaRealm &>/dev/null

$PAYARA_PATH/asadmin deploy --force --type osgi glassfish-jdbc-realm/target/glassfish-jdbc-realm*jar &> /dev/null

$PAYARA_PATH/asadmin create-jdbc-connection-pool \
    --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource \
    --restype javax.sql.ConnectionPoolDataSource \
    --property User=mt:Password=mt:DatabaseName=mt:ServerName=localhost \
    MtPool &> /dev/null

$PAYARA_PATH/asadmin create-jdbc-connection-pool \
    --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource \
    --restype javax.sql.ConnectionPoolDataSource \
    --property User=mtms:Password=mtms:DatabaseName=mtms:ServerName=localhost \
    MtmsPool &> /dev/null

$PAYARA_PATH/asadmin create-jdbc-resource --connectionpoolid MtPool jdbc/mt &> /dev/null
$PAYARA_PATH/asadmin create-jdbc-resource --connectionpoolid MtmsPool jdbc/mtms &> /dev/null

$PAYARA_PATH/asadmin create-auth-realm \
    --classname nl.mb.glassfish.realm.CustomJdbcUserRealm \
    --property jaas-context=CustomJdbcUserRealm:datasource-jndi=jdbc/mt:digest-algorithm=SHA-256:digest-encoding=hex:password-charset=UTF-8:password-query=select\ u.password\ from\ allusers\ u\ where\ u.email\\=?:security-roles-query=select\ g.name\ from\ alluser_groups\ ug\ inner\ join\ allusers\ u\ on\ ug.email\\=u.email\ and\ u.email\\=?\ inner\ join\ allgroups\ g\ on\ ug.group_id\\=g.id \
    MultitenantRealm &> /dev/null

$PAYARA_PATH/asadmin create-auth-realm \
    --classname nl.mb.glassfish.realm.CustomJdbcUserRealm \
    --property jaas-context=CustomJdbcUserRealm:datasource-jndi=jdbc/mtms:digest-algorithm=SHA-256:digest-encoding=hex:password-charset=UTF-8:password-query=select\ u.password\ from\ security.allusers\ u\ where\ u.email\\=?:security-roles-query=select\ g.name\ from\ security.alluser_groups\ ug\ inner\ join\ security.allusers\ u\ on\ ug.email\\=u.email\ and\ u.email\\=?\ inner\ join\ security.allgroups\ g\ on\ ug.group_id\\=g.id \
    MultitenantMultiSchemaRealm &> /dev/null

echo The server is ready.