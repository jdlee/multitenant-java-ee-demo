CREATE OR REPLACE VIEW orders AS
    SELECT * FROM allorders WHERE comp_id = CAST (get_var('comp_id') AS NUMERIC);

CREATE TRIGGER set_order_company BEFORE INSERT OR UPDATE ON allorders
    FOR EACH ROW EXECUTE PROCEDURE set_company();
