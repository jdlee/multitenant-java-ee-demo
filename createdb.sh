#!/bin/bash

function sharedSchemaVar() {
    echo -e "*****Creating shared schema database"
    dropdb --if-exists mt 
    createdb mt
    psql --quiet mt << EOF
        drop role if exists mt;
        create user mt with password 'mt';
        alter database mt owner to mt;
        grant all on database mt to mt;
EOF

    psql --quiet -U mt mt -f setup.sql
    cat orders.sql | sed -e 's/orders/allorders/' | psql --quiet -U mt mt
    psql --quiet -U mt mt -f views.sql
    psql --quiet -U mt mt -f users.sql
    psql --quiet -U mt mt -f comp1.sql
    psql --quiet -U mt mt -f comp2.sql
}

function separateSchema() {
    echo -e "*****Creating separate schema databases"
    dropdb --if-exists mtms
    createdb mtms
    psql --quiet mtms << EOF
        drop role if exists mtms;
        drop role if exists comp1;
        drop role if exists comp2;

        create user mtms with password 'mtms';
        create user comp1 with password 'comp1';
        create user comp2 with password 'comp2';

        grant all on database mtms to mtms;
        ALTER DATABASE mtms OWNER TO mtms;
EOF

    psql --quiet -U mtms mtms << EOF
        create schema security;
        create schema comp1;
        create schema comp2;

        set schema 'comp1';
        \i orders.sql
        \i comp1.sql
        ALTER TABLE orders ALTER COLUMN comp_id SET DEFAULT 1;

        set schema 'comp2';
        \i orders.sql
        \i comp2.sql
        ALTER TABLE orders ALTER COLUMN comp_id SET DEFAULT 2;

        set schema 'security';
        \i users.sql
EOF
}

function separateDatabase() {
    echo -e "*****Creating separate databases"
    dropdb --if-exists mtcomp1
    dropdb --if-exists mtcomp2
    dropuser --if-exists mtcomp1
    dropuser --if-exists mtcomp2

    createuser mtcomp1
    createuser mtcomp2
    createdb mtcomp1 -O mtcomp1
    createdb mtcomp2 -O mtcomp2

    psql --quiet -U mtcomp1 mtcomp1 -f orders.sql
    psql --quiet -U mtcomp1 mtcomp1 -f users.sql
    psql --quiet -U mtcomp1 mtcomp1 -f comp1.sql
    psql --quiet -U mtcomp1 << EOF
    ALTER TABLE orders ALTER COLUMN comp_id SET DEFAULT 1;
EOF

    psql --quiet -U mtcomp2 mtcomp2 -f orders.sql
    psql --quiet -U mtcomp2 mtcomp2 -f users.sql
    psql --quiet -U mtcomp2 mtcomp2 -f comp2.sql
    psql --quiet -U mtcomp2 << EOF
    ALTER TABLE orders ALTER COLUMN comp_id SET DEFAULT 2;
EOF
}

PAYARA_PATH=payara41/bin
if [ -e $PAYARA_PATH/asadmin ] ; then
    $PAYARA_PATH/asadmin stop-domain domain1
fi

sharedSchemaVar
separateSchema
separateDatabase
