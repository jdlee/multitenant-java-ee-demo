DROP TABLE IF EXISTS orders;

CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    comp_id INT,
    customer INT NOT NULL,
    orderdate TIMESTAMP DEFAULT now());

